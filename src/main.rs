extern crate time;
extern crate rustc_version_runtime;

use std::fs::File;
use std::fs;
use std::io::prelude::*;
use std::io::BufReader;
use std::time::Instant;

const RUN_TIMES: usize = 200000_usize;

fn main() {
    // Version number
    let version_ = rustc_version_runtime::version();
    let mut version = version_.major.to_string();
    version.push_str(".");
    version.push_str(&version_.minor.to_string());
    version.push_str(".");
    version.push_str(&version_.patch.to_string());

    // Filename to use for temp file
    let filename = "/tmp/rust_read_test";

    // Buffersize to use
    //let bufferSize = 1024;

    run_test(&filename,1024);
    run_test(&filename,2048);
}

fn run_test(filename: &str, size: u32){
    println!("Running test on file size: {}", size);
    temp_file(&filename, size);
    let mut total_time: f64 = 0 as f64;
    println!("Begun test");
    for _ in 0..RUN_TIMES{
        total_time += buf_reader(&filename);
    }
    println!("End test");
    println!("BufReader - Mean time: {}s", total_time / (RUN_TIMES as f64));


    let mut total_time: f64 = 0 as f64;
    println!("Begun test");
    for _ in 0..RUN_TIMES{
        total_time += read_string(&filename);
    }
    println!("End test");
    println!("Read_to_string - Mean time: {}s", total_time / (RUN_TIMES as f64));
    temp_remove(&filename);
}

fn temp_file(filename: &str, size: u32){
    let mut file = File::create(filename).expect("Couldn't create file");

    for _ in 0..size {
        file.write(b"1").expect("Couldn't write data");
    }
    file.flush().expect("Couldn't flush");
    println!("Test file containing {} bytes created", size)
}
fn temp_remove(filename: &str){
    fs::remove_file(filename).expect("Couldn't remove file");
    println!("Temp file is removed");
}
fn read_string(filename: &str) -> f64 {
    let mut file = File::open(filename).expect("Couldn't open file");

    let now = Instant::now();
    let mut buffer = String::new();
    file.read_to_string(&mut buffer).expect("Something wrong");
    let elapsed = now.elapsed();
    let sec = (elapsed.as_secs() as f64) + (elapsed.subsec_nanos() as f64 / 1000_000_000.0);
    return sec;
}
fn buf_reader(filename: &str) -> f64 {
    let file = File::open(filename).expect("Couldn't open file");

    let now = Instant::now();
    let capacity = match file.metadata() {
        Ok(metadata) => metadata.len(),
        Err(_) => 0,
    };

    let mut reader = BufReader::with_capacity(capacity as usize, file);
    let mut file_contents = String::with_capacity(capacity as usize);
    match reader.fill_buf() {
        Ok(buf) => {
            match ::std::str::from_utf8(buf) {
                Ok(s) => file_contents.push_str(s),
                Err(_) => {
                }
            };
        }
        Err(_) => {

        }
    };
    let elapsed = now.elapsed();
    let sec = (elapsed.as_secs() as f64) + (elapsed.subsec_nanos() as f64 / 1000_000_000.0);
    return sec;
}
